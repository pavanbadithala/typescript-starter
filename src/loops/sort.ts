const sort = (arr: ReadonlyArray<number>): ReadonlyArray<number> => {
  let temp = 0;
  for (let i = 0; i < arr.length; i++) {
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[i] > arr[j]) {
        temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
      }
    }
  }
  return arr;
};
console.log(sort([80, 90, 10, 50, 40, 1, 2, 5, 5]));
