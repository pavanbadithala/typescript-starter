const unique = (arr1: ReadonlyArray<number>): ReadonlyArray<number> => {
  const push = (
    arr: ReadonlyArray<number>,
    e: number
  ): ReadonlyArray<number> => [...arr, e];

  let result: ReadonlyArray<number> = [arr1[0]];
  for (let i = 1; i < arr1.length; i++) {
    if (arr1[i] !== arr1[i - 1]) {
      result = push(result, arr1[i]);
    }
  }
  return result;
};
console.log(unique([1, 2, 5, 8, 8, 10, 5, 8, 80, 90, 20]));
