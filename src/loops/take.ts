const takewhile = (
  arr: ReadonlyArray<number>,
  f: (x: number) => boolean
): ReadonlyArray<number> => {
  const Take = [];
  for (const e of arr) {
    if (f(e)) {
      Take.push(e);
    }
    return Take;
  }
};
console.log(takewhile([2, 4, 6, 9], x => x % 2 === 0));
