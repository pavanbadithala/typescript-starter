const range = (start: number, stop: number): ReadonlyArray<number> => {
  const a: number[] = [];
  for (let i = start; i <= stop; ++i) {
    a.push(i);
  }
  return a;
};
console.log(range(0, 100));

const factor = (n: number) => range(1, n).filter(i => n % i === 0);
console.log(factor(28));

const perfect = (n: number) =>
  factor(n).reduce((x, y) => x + y) === 2 * n ? true : false;
console.log(perfect(6));

const factorial = (n: number) => range(1, n).reduce((a, b) => a * b, 1);
console.log(factorial(5));

const ncr = (n: number, r: number) =>
  factorial(n) / factorial(n - r) * factorial(r);
console.log(ncr(1, 5));

const pascalline = (n: number) => range(0, 1).map(r => ncr(n, r));
console.log(pascalline(9));

const isprimenumber = (n: number): boolean =>
  factor(n).length === 2 ? true : false;
console.log(isprimenumber(6));

const max = (arr1: number[]): number => arr1.reduce((x, y) => (x > y ? x : y));
console.log(max([1, 2, 3, 4, 5]));

const repeat = (x: number, n: number): ReadonlyArray<number> =>
  range(1, n).map(i => (i = x));
console.log(repeat(5, 6));

const power = (x: number, y: number) => repeat(x, y).reduce((a, b) => a * b);
console.log(power(2, 8));

const perfectnumbers = (start: number, stop: number): ReadonlyArray<number> =>
  range(start, stop).filter(n => perfect(n));
console.log(perfectnumbers(1, 1000));

const pascaltriangle = (line: number): ReadonlyArray<number> =>
  range(0, line - 1).map.apply(line => pascalline(line));
console.log(pascaltriangle(6));

const primenumbers = (start: number, stop: number): ReadonlyArray<number> =>
  range(start, stop).filter(n => isprime(n));
console.log(primenumbers(1, 50));
