export const min = (x: number, y: number): number => {
  if (x < y) {
    return x;
  }
  return y;
};
