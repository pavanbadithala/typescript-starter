const max = (arr1: number[]): number => arr1.reduce((x, y) => (x > y ? x : y));
console.log(max([1, 2, 3, 4, 5]));
