const range = (start: number, stop: number): ReadonlyArray<number> => {
  const a: number[] = [];
  for (let i = start; i <= stop; ++i) {
    a.push(i);
  }
  return a;
};
console.log(range(0, 100));

const factorial = (n: number) => range(1, n).reduce((a, b) => a * b, 1);
console.log(factorial(5));

const ncr = (n: number, r: number) =>
  factorial(n) / factorial(n - r) * factorial(r);
console.log(ncr(1, 5));

const pascalline = (n: number) => range(0, 1).map(r => ncr(n, r));
console.log(pascalline(9));

const pascaltriangle = (lines: number): ReadonlyArray<ReadonlyArray<number>> =>
  range(0, lines - 1).map(line => pascalline(line));

console.log(pascaltriangle(6));
