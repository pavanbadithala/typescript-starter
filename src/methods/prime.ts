const range = (start: number, stop: number): ReadonlyArray<number> => {
  const a: number[] = [];
  for (let i = start; i <= stop; ++i) {
    a.push(i);
  }
  return a;
};
console.log(range(0, 100));

const factor = (n: number) => range(1, n).filter(i => n % i === 0);
console.log(factor(28));

const isprimenumber = (n: number): boolean =>
  factor(n).length === 2 ? true : false;
console.log(isprimenumber(6));

const primenumbers = (start: number, stop: number): ReadonlyArray<number> =>
  range(start, stop).filter(n => isprimenumber(n));
console.log(primenumbers(1, 50));
