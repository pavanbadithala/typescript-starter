const range = (start: number, stop: number): ReadonlyArray<number> => {
  const a: number[] = [];
  for (let i = start; i <= stop; ++i) {
    a.push(i);
  }
  return a;
};
console.log(range(0, 100));

const perfect = (n: number) =>
  factor(n).reduce((x, y) => x + y) === 2 * n ? true : false;
console.log(perfect(6));

const perfectnumbers = (start: number, stop: number): ReadonlyArray<number> =>
  range(start, stop).filter(n => perfect(n));
console.log(perfectnumbers(1, 1000));
